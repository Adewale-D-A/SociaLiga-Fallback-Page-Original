import React from 'react';
import './App.css';
import {ButtonComponent} from './components/button';
import {ContentOne} from './components/content-one';
import {ContentTwo} from './components/content-two';
import {NavBar} from './components/nav-bar';
import {NavBarResponsive} from './components/nav-bar-responsive';


function App() {
  return (
    <>
      <div className='nav-pos-responsive'>
        <NavBarResponsive/>
      </div>
      <div className='nav-pos'>
        <NavBar/>
      </div>
      <div className='main-body'>
        <div className='content-one-body'>
          <ContentOne/>        
        </div>
        <div className='content-two-body'>
          <ContentTwo/>        
        </div>
        <div className='content-three-body'> 
          <ButtonComponent/>         
        </div>  
      </div>
    </>
  );
}

export default App;
