import React from 'react';
import './css/button.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

export const ButtonComponent = () => {
    return (
        <>
            <div>
                <button className='fallback-btn' type='button' value="Return Back">
                    <span className='btn-container-2'>
                        <FontAwesomeIcon icon={faArrowLeft} className="icon-awe"/>
                        <span className='btn-value'>Return</span>
                    </span>
                </button>
            </div>
        </>
    )
}

export default ButtonComponent;