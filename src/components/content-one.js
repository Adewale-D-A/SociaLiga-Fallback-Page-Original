import React from 'react';
import './css/content-one.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleCheck} from '@fortawesome/free-solid-svg-icons';
import 'animate.css';

export const ContentOne = () => {
    return (
        <>
            <div>
                <div className='content-one'>
                    <div className='content-container-2 animate__animated animate__bounce'>
                        <h2 className='content-value'>Congratulations!</h2>
                        <FontAwesomeIcon icon={faCircleCheck} className="icon-awe-check"/>
                    </div>
                    <div className='mini-content'>
                        <p>Your account has been successfully set up.</p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ContentOne;