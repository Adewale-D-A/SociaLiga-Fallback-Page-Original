import React from 'react';
import './css/content-two.css';

export const ContentTwo= () => {
    return (
        <>
            <div>
                <div className='content-two'>
                    <div className='content-two-value'>
                        <h2 className='content-two-value-a'>Thank You!</h2>
                        <p className='content-two-value-b'>for choosing SociaLiga</p>
                    </div>
                    <div className='content-container-two'>
                        <h3 className='content-container-two-a'>Where sport meets great entertainment</h3>
                        <p className='content-container-two-b'> 
                            We bring people of all ages together through 
                            sport and entertainment. it’s all about fun, 
                            networking and more fun.
                        </p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ContentTwo;