import React from 'react';
import './css/nav-bar.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass, faCartShopping, faCaretDown} from '@fortawesome/free-solid-svg-icons';

export const NavBar = () => {
    return (
        <>
            <div>
                <div className='NavBar'>
                    <div className='logo-container'>
                        <span className='logo-container-cont'>
                            <img src="./staticIMG/SociaLiga.png" className='logo-img' alt='logo-png'/>
                            <span className='main-name'>SociaLiga</span>
                        </span>
                    </div>
                    <div className='nav-bar-links'>
                        <div className='nav-links-items'>
                            <div className='dropdown'>
                                <div className='comp-container'>
                                    <button className='company-link dropbtn'> COMPANY <FontAwesomeIcon icon={faCaretDown} className="icon-awe-caret"/></button>
                                </div>                                
                                <div class="dropdown-content">
                                    <span className='link-att'><a href="https://www.thesocialiga.com/about/">About The SociaLiga</a></span>
                                    <span className='link-att'><a href="http://thesocialigafoundation.com/">The SociaLiga Foundation</a></span>
                                    <span  className='link-att'><a href="https://www.thesocialiga.com/contact/">Contact</a></span>
                                </div>
                            </div>
                            <div className='dropdown'>
                                <a href='https://www.thesocialiga.com/shop/'>
                                    <div className='comp-container'>                            
                                        <button className='shop-link dropbtn'> SHOP </button> 
                                    </div>
                                </a>
                            </div>                           
                            <div className='dropdown'>
                                <div className='comp-container'>
                                    <button className='join-us-link dropbtn'> JOIN US <FontAwesomeIcon icon={faCaretDown} className="icon-awe-caret"/></button>
                                </div>                                
                                <div class="dropdown-content">
                                    <span className='link-att'><a href="https://www.thesocialiga.com/careers/">Careers</a></span>
                                    <span className='link-att'><a href="https://www.thesocialiga.com/join/">SociaLiga Community</a></span>
                                    <span  className='link-att'><a href="https://www.thesocialiga.com/vendors/">Vendor Community</a></span>
                                    <span  className='link-att'><a href="https://www.thesocialiga.com/creatives/">Creatives Community</a></span>
                                    <span  className='link-att'><a href="https://www.thesocialiga.com/newteams/">Create Your SociaLiga Team</a></span>
                                    <span  className='link-att'><a href="https://www.thesocialiga.com/resellers/">Reseller Community</a></span>
                                </div>    
                            </div>
                            <div className='dropdown'>
                                <a href='https://www.thesocialiga.com/faq/'>
                                    <div className='comp-container'>
                                        <button className='faqs-link dropbtn'> FAQS </button>
                                    </div> 
                                </a>
                            </div>                              
                            <div className='dropdown'>
                                <div className='comp-container'>
                                    <button className='blog-link dropbtn'> BLOG <FontAwesomeIcon icon={faCaretDown} className="icon-awe-caret"/></button>
                                </div>
                                <div class="dropdown-content">
                                    <span className='link-att'><a href="https://www.thesocialiga.com/quiz/">Quiz</a></span>
                                    <span className='link-att'><a href="https://www.thesocialiga.com/moneytalk/">Money Talk</a></span>
                                    <span  className='link-att'><a href="https://www.thesocialiga.com/naijalife/">Naija Life</a></span>
                                    <span  className='link-att'><a href="https://www.thesocialiga.com/pop/">Pop Culture</a></span>
                                </div>   
                            </div>
                            
                        </div>
                       <div className='nav-icons'> 
                            <span className='search'>  <FontAwesomeIcon icon={faMagnifyingGlass} className="icon-awe-search"/> </span>
                            <a href='https://www.thesocialiga.com/cart/'>
                            <span className='cart'>  <FontAwesomeIcon icon={faCartShopping} className="icon-awe-cart"/> </span>
                            </a>
                       </div>                        
                    </div>
                </div>
            </div>
        </>
    )
}

export default NavBar;