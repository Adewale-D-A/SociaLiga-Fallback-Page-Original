import React from 'react';
import './css/togglerefresh.css';
import {TemporaryDrawer} from './sidebar';

export const ReloadRefresh = () => {
    const reload = () => {
        document.location.reload()
    }
    return (
        <>
            <div>
                <div className='NavBar-Responsive'>
                    <div className='logo-container-responsive'>
                        <span className='logo-container-cont-responsive'>
                            <img src="./staticIMG/SociaLiga.png" className='logo-img-responsive' alt='logo-png'/>
                            <span className='main-name-responsive'>SociaLiga</span>
                        </span>
                    </div>
                    <div className='nav-bar-links-responsive'>
                        <div className='nav-links-items-responsive'>
                            <div onClick={reload}>
                                <button className='menu-bar-responsive' type='button' value="Icon">
                                    <span className='click-menu-bar-responsive'>
                                        <span className='menu-bar-value'><TemporaryDrawer/></span>
                                    </span>
                                </button>                                
                            </div>
                        </div>                     
                    </div>
                </div>
            </div>
        </>
    )
}

export default ReloadRefresh;