import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass, faCartShopping, faCaretDown, faBars} from '@fortawesome/free-solid-svg-icons';
import 'bootstrap/dist/css/bootstrap.css';
import Accordion from 'react-bootstrap/Accordion';
import './css/sidebar.css';
import {ReloadRefresh} from './togglerefresh';

export const TemporaryDrawer = () => {
  const [state, setState] = React.useState({
    top: false
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    
    <div className='main-content'>
      <ReloadRefresh/>
      <Box
        role="presentation"
        onKeyDown={toggleDrawer(anchor, false)}>
        <List>
              <ListItem>
                <div className='search-cart-items'>
                      <div className='search-item'>
                          <FontAwesomeIcon icon={faMagnifyingGlass} className="search-icon"/>
                          <input placeholder='Search' className='search-ite'></input>
                      </div>
                      <div>
                              <FontAwesomeIcon icon={faCartShopping} className="cart-item"/>
                      </div>
                </div>
              </ListItem>     
                    <Accordion defaultActiveKey="0">      
              <ListItem >                
                        <Accordion.Item eventKey="0" id="company">
                            <Accordion.Header >
                                <div id='comp-icon'>
                                  COMPANY
                                  <FontAwesomeIcon icon={faCaretDown} id="ico-starts"/>
                                </div>
                            </Accordion.Header>
                            <Accordion.Body>
                              <div>
                                <a href="https://www.thesocialiga.com/about/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      About The SociaLiga
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>
                              <div>
                                <a href="http://thesocialigafoundation.com/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      The SociaLiga Foundation
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>
                              <div>
                                <a href="https://www.thesocialiga.com/contact/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      Contact
                                    </span>
                                  </ListItem > 
                                </a>
                              </div>                              
                            </Accordion.Body>
                        </Accordion.Item>
                    {/* </Accordion> */}
              </ListItem>            
              <Divider id ='divider-1'/>
              <a href='https://www.thesocialiga.com/shop/'>
                <ListItem id="company">
                  <div>                  
                    <h4 style={{fontSize:"13.2px"}}>SHOP</h4>
                  </div>
                </ListItem>   
              </a>         
              <Divider id ='divider-1'/>
              <ListItem >                
                    {/* <Accordion defaultActiveKey="0" alwaysOpen="False" flush> */}
                        <Accordion.Item eventKey="1" id="company">
                            <Accordion.Header >
                                <div id='comp-icon'>
                                  JOIN US
                                  <FontAwesomeIcon icon={faCaretDown} id="ico-starts"/>
                                </div>
                            </Accordion.Header>
                            <Accordion.Body>
                              <div>
                                <a href="https://www.thesocialiga.com/careers/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      Careers
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>
                              <div>
                                <a href="https://www.thesocialiga.com/join/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      SociaLiga Community
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>
                              <div>
                                <a href="https://www.thesocialiga.com/vendors/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      Vendor Community
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>  
                              <div>
                                <a href="https://www.thesocialiga.com/creatives/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      Creative Community
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>
                              <div>
                                <a href="https://www.thesocialiga.com/newteams/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      Creative Your SociaLiga Team
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>

                              <div>
                                <a href="https://www.thesocialiga.com/resellers/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      Reseller Community
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>                              
                            </Accordion.Body>
                        </Accordion.Item>
                    {/* </Accordion> */}
              </ListItem>       
              <Divider id ='divider-1'/>
              <a href='https://www.thesocialiga.com/faq/'>
                <ListItem id="company">
                  <div>                  
                    <h4 style={{fontSize:"13.2px"}}>FAQS</h4>
                  </div>
                </ListItem>  
              </a>    
              <Divider id ='divider-1'/>
              <ListItem >                
                    {/* <Accordion defaultActiveKey="0" alwaysOpen="False" flush> */}
                        <Accordion.Item eventKey="2" id="company">
                            <Accordion.Header >
                                <div id='comp-icon'>
                                  BLOG
                                  <FontAwesomeIcon icon={faCaretDown} id="ico-starts"/>
                                </div>
                            </Accordion.Header>
                            <Accordion.Body>
                              <div>
                                <a href="https://www.thesocialiga.com/quiz/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      QUIZ
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>
                              <div>
                                <a href="https://www.thesocialiga.com/moneytalk/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      Money Talk
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>
                              <div>
                                <a href="https://www.thesocialiga.com/naijalife/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      Naija Life
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a>
                              </div>  
                              <div>
                                <a href="https://www.thesocialiga.com/pop/">
                                  <ListItem  id='list-item'>  
                                    <span>
                                      Pop Culture
                                    </span>
                                  </ListItem > 
                                  <Divider id ='divider-2'/> 
                                </a> 
                              </div>                              
                            </Accordion.Body>
                        </Accordion.Item>
              </ListItem>     
              
              </Accordion>             
        </List>
      </Box>
    </div>
  );

  return (
    <div className='main-item-container'>
        {['top'].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button onClick={toggleDrawer(anchor, true)}><FontAwesomeIcon icon={faBars} id="bars-menu"/></Button>
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            <div>                
                {list(anchor)}
            </div>
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}

export default TemporaryDrawer;